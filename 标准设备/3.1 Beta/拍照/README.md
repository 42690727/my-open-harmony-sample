# 拍照
## 1.样例简介
在OpenHarmony标准设备上，开发一个应用程序，通过调用linux自带通用GPIO驱动，实现点击按钮触发拍照的功能。



## 2.涉及技术特性
- linux gpio 输入输出设置，中断响应
- 使用拍照子系统拍照

## 3.支持OpenHarmony版本
- OpenHarmony 3.1 Beta


## 4.验证开发板
- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件


## 5.上手指南
#### 5.1 下载OpenHarmony3.1 beta代码

```
repo init -u git@gitee.com:openharmony/manifest.git -b refs/tags/OpenHarmony-v3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 5.2 拷贝应用源码

将样例src代码拷贝到OpenHarmony源码根目录下

#### 5.3 修改配置，增加编译组件

修改applications\standard\hap\ohos.build

module_list里增加 "//applications/standard/app:madixin"

#### 5.4 重新编译和烧录

```
./build.sh --product-name Hi3516DV300 --ccache
```

#### 5.5 执行验证

使用串口连接，执行bin目录下的madixin

```
cd bin
./madixin
```



## 6. 详细文章介绍

 [鸿蒙OpenHarmony hi3516开发板，标准系统响应按钮拍照](https://blog.csdn.net/sd2131512/article/details/122179733)