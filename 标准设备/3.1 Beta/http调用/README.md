# http接口调用
## 1.样例简介
在OpenHarmony标准设备上，开发一个应用程序，通过调用OpenHarmony已集成的libcurl，封装2个方法，实现对外网http reset接口（get/post）调用。作为后期调用AI云服务的基础。



## 2.涉及技术特性
- linux libcurl调用云服务

## 3.支持OpenHarmony版本
- OpenHarmony 3.1 Beta


## 4.验证开发板
- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件


## 5.上手指南
#### 5.1 下载OpenHarmony3.1 beta代码

```
repo init -u git@gitee.com:openharmony/manifest.git -b refs/tags/OpenHarmony-v3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 5.2 拷贝应用源码

将样例src代码拷贝到OpenHarmony源码根目录下

#### 5.3 修改配置，增加编译组件

修改applications\standard\hap\ohos.build

module_list里增加 "//applications/standard/httptest:httptest"

#### 5.4 重新编译和烧录

```
./build.sh --product-name Hi3516DV300 --ccache
```

#### 5.5 DNS修改
目前发现如果请求外部网络是带域名时，会报“无法解析代理”的错误，因此当前只能手动配置DNS服务，或者在配置修改后再编译。这里只写出镜像编译后的修改方式。
```
mount -oremount,rw  /    #使etc目录下可写
echo "nameserver 202.96.128.86">/etc/resolv.conf  #写入DNS服务地址
```

#### 5.6 执行验证

使用串口连接，执行bin目录下的httptest

```
cd bin
./httptest
```

## 6. 详细文章介绍

 [鸿蒙OpenHarmony hi3516开发板，标准系统调用外部Rest接口](https://blog.csdn.net/sd2131512/article/details/122391420)