
#ifndef BAIDU_CLOUD_H
#define BAIDU_CLOUD_H


#include<stdio.h>
#include<curl/curl.h>
#include <fstream>
#include <sstream>
#include<string>
#include "json/json.h"
#include "base64.h"


class BdCloud {
public:
    // http get
    CURLcode httpGet(const std::string & strUrl, std::string & strResponse,int nTimeout);
    // htpp post 
    CURLcode httpPost(const std::string & strUrl, std::string szJson,std::string & strResponse,int nTimeout);

    CURLcode getToken(std::string & strToken);
    CURLcode recognizeText(const std::string file,std::string & strResponse,const std::string strToken);

    

private:
    static size_t receive_data(void *contents, size_t size, size_t nmemb, void *stream);
    static std::string readFileIntoString(const std::string filename);


    const std::string bdCloudTokenUrl = "https://aip.baidubce.com/oauth/2.0/token";
    const std::string bdCloudTextUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic";
    const std::string bdCloudAK = "xF3i29srDM5BSku4ZUYAsVrb";
    const std::string bdCloudSK = "uvLo3E9o0saZV95qkRUCqeMIKme8yIss";

};


#endif // BAIDU_CLOUD_H
