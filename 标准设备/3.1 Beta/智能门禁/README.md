# 智能门禁
## 1.样例简介
在OpenHarmony标准设备上，开发一个应用程序，实现智能门禁的效果（暂时以闪灯表示门禁识别）。

暂时没有拍完整操作的图，大家自己想象下吧。。。

![demo](https://img-blog.csdnimg.cn/e4cde8bcf23b489d82e7119ec000f22c.png)

1.1 准备文字(车牌)打印在白纸上，文字对准摄像头，模拟汽车靠近门禁。

1.2 点击3516开发板上自定义按钮触发拍照，调用百度云识别车牌。

1.3 针对识别结果，如果文字时当前车主的车牌号，则闪3下粉色灯，如果不是，则闪3下红灯。模拟设备响应车牌识别结果。

## 2.涉及技术特性

- linux gpio 输入输出设置，中断响应
- 标准设备拍照
- 网络rest http调用(调用百度云文字识别接口)

## 3.支持OpenHarmony版本

- OpenHarmony 3.1 Beta


## 4.验证开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件

## 5.方案
最近很懒。。。懒得画图了，用文字描述一下。。。

5.1 准备文字(车牌)打印在白纸上，文字对准摄像头，模拟汽车靠近门禁

5.2 通过调用linux自带通用GPIO驱动，实现点击自定义按钮（GPIO中断）

5.3 按钮触发摄像头拍照

5.4 把照片内容base64编码，并调用百度云文字识别服务，识别文字内容，参考https://blog.csdn.net/sd2131512/article/details/122372787

5.5 针对识别结果，如果文字时当前车主的车牌号，则闪3下粉色灯，如果不是，则闪3下红灯。同时后台打印日志，显示识别出的文字。模拟设备响应车牌识别结果。 之所以没用绿灯响应识别成功，好像绿灯会与摄像头拍照有些冲突，待定位。。。


## 5.上手指南
#### 5.1 下载OpenHarmony3.1 beta代码

```
repo init -u git@gitee.com:openharmony/manifest.git -b refs/tags/OpenHarmony-v3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 5.2 拷贝应用源码

将样例src代码拷贝到OpenHarmony源码根目录下

#### 5.3 修改配置，增加编译组件

修改applications\standard\hap\ohos.build

module_list里增加 "//applications/standard/aiGuard:aiGuard"

#### 5.4 重新编译和烧录

```
./build.sh --product-name Hi3516DV300 --ccache
```

#### 5.5 执行验证

使用串口连接，执行bin目录下的hello

```
cd bin
./aiGuard
```



## 6. 详细文章介绍

 [鸿蒙OpenHarmony hi3516开发板，标准系统实现智能门禁](https://blog.csdn.net/sd2131512/article/details/122460311)