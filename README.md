# 我的OpenHarmony开发样例

记录和分享自己学习OpenHarmony时，写的开发样例。



更多官方开发样例，请参考：



## 轻量设备





## 小型设备





## 标准设备

#### 3.1 Beta版本
- [gpio点灯](标准设备/3.1%20Beta/gpio点灯)
- [拍照](标准设备/3.1%20Beta/拍照)
- [http调用](标准设备/3.1%20Beta/http调用)
- [智能门禁](标准设备/3.1%20Beta/智能门禁)

#### 3.2
- [ArkTS图片选择](标准设备/3.2/FilePickerTest)


## 相关资源
#### 3.1 Beta版本发布会 (2021-12-31)
- [OpenHarmony 3.1 Beta版本关键特性及深度技术文章介绍1230](https://docs.qq.com/slide/DY2pwUnNzYXhPTW9O)
- [OH Dev-Board-SIG专题材料-1230](https://docs.qq.com/slide/DY0ZpaXZqc1BHS2JQ)
- [OpenHarmony 3.1Beta版本开发者样例](https://docs.qq.com/slide/DVmNmdGlmbU9ZVUFu)
- [1230版本讲解会议](https://meeting.tencent.com/v2/cloud-record/share?id=2e312d9b-36c0-4561-828c-f2a7e5d9d217&from=3)
